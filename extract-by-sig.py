#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pickle
import sys
from collections import defaultdict

if len(sys.argv) < 2 or not os.path.isfile(sys.argv[1]):
    sys.exit('you must specify a pickle file to load')

with open(sys.argv[1], 'rb') as fd:
    pulls = pickle.load(fd)

# print('number,user,assignee,created_at,closed_at,sigs,review_s')
months = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))
for pr in pulls.values():
    sigs = frozenset([l.name for l in pr.labels if l.name.startswith('sig/')])
    created = "{}-{:02d}".format(pr.created_at.year, pr.created_at.month)
    closed = "{}-{:02d}".format(pr.closed_at.year, pr.closed_at.month)
    months[created][sigs]['created'] += 1
    months[closed][sigs]['closed'] += 1
#    print(pr.number, pr.user.login, assignee, pr.created_at, pr.closed_at, ' '.join(sigs), review_s, sep=',')
for month in sorted(months.keys()):
    pass
