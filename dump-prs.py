#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pickle
import time
from datetime import datetime, timedelta

from github import Github

with open('TOKEN') as fd:
    g = Github(fd.read().strip())

pulls = {}
added = 1
date = datetime.now()
try:
    while added > 0:
        added = 0
        newdate = date - timedelta(days=15)
        query = 'repo:kubernetes/kubernetes merged:{:d}-{:02d}-{:02d}..{:d}-{:02d}-{:02d}'.format(
            newdate.year, newdate.month, newdate.day,
            date.year, date.month, date.day
        )
        print('Adding issues for query:', query)
        for pr in g.search_issues(query):
            pulls[pr.number] = pr
            added += 1
        print(added, 'issues added, sleeping 60s...')
        if date.year < 2020:
            break
        time.sleep(60)
        date = newdate
finally:
    print('got %d issues' % len(pulls))
    with open('github.pickle', 'wb') as fd:
        pickle.dump(pulls, fd)
