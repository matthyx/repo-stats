from datetime import datetime, timedelta

date = datetime.now()
for i in range(20):
    newdate = date - timedelta(days=30)
    print('merged:{:d}-{:02d}-{:02d}..{:d}-{:02d}-{:02d}'.format(
        newdate.year, newdate.month, newdate.day,
        date.year, date.month, date.day
    ))
    date = newdate
