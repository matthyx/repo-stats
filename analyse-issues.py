#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pickle
import sys
from collections import defaultdict

if len(sys.argv) < 2 or not os.path.isfile(sys.argv[1]):
    sys.exit('you must specify a pickle file to load')

with open(sys.argv[1], 'rb') as fd:
    pulls = pickle.load(fd)

users = defaultdict(int)
assignees = defaultdict(lambda: defaultdict(int))
for pr in pulls.values():
    users[pr.user.login] += 1
    for a in pr.assignees:
        assignees[pr.user.login][a.login] += 1

for u in sorted(users, key=lambda u: users[u]):
    print(users[u], u)
    for a in sorted(assignees[u], key=lambda a: assignees[u][a], reverse=True)[:5]:
        print('\t', assignees[u][a], a)
