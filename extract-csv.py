#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import glob
import pickle

pulls = {}
for f in glob.glob('*.pickle'):
    print('Reading', f)
    with open(f, 'rb') as fd:
        pulls.update(pickle.load(fd))

print('number,user,assignee,created_at,closed_at,sigs,review_s')
for pr in pulls.values():
    try:
        assignee = pr.assignee.login
    except AttributeError:
        assignee = None
    sigs = []
    for l in pr.labels:
        if l.name.startswith('sig/'):
            sigs.append(l.name)
    review_s = (pr.closed_at - pr.created_at).seconds
    print(pr.number, pr.user.login, assignee, pr.created_at,
          pr.closed_at, ' '.join(sigs), review_s, sep=',')
