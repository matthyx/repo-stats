# Kubernetes repository statistics
This repo contains some helper scripts to retrieve merged PRs from k/k
in the [PyGithub](https://pygithub.readthedocs.io/en/latest/index.html)
format and extract some statistics.

PRs are persisted in `pickle` format to limit the queries done to Github.

## Requirements
You need `python3` and some requirements that can be installed using pip:

```
$ pip3 install -r requirements.txt
```

You also need a read-only token for GitHub:
1. go to https://github.com/settings/tokens
1. click `Generate new token`
1. make sure you select no scope at all
1. save the generated token in a file named `TOKEN` (it is only visible once!)

## Approved PRs by a specific user in the past 3 months
You can call this standalone script to get all PRs where a particular user has
issued the command `/approve` in a comment.

```
$ ./approved-prs.py matthyx
Approved PRs by matthyx in kubernetes/kubernetes
matthyx https://github.com/kubernetes/kubernetes/pull/85234#issuecomment-581791019
(parsed 296 events, max 300 or 90 days)
```

GitHub only allows searching for the last 300 events, or all events occured in the
last 90 days. The last line tells you which limit was reached.

## Getting merged PRs in k/k from Github
All other scripts require `pickle` files containing PRs to function.

Either you download these files from [here](http://matthiasbertschy.info/kubernetes/) or you fetch them using this script:

```
$ ./dump-prs.py 
Adding issues for query: repo:kubernetes/kubernetes merged:2020-03-26..2020-04-10
260 issues added, sleeping 60s...
...
```

- the script will fetch all merged PRs from the last 15 days
- then it sleeps 60s and do the next step
- when it reaches the end of 2019, it stops

We do like that to avoid reaching the 1'000 search limit of GitHub per minute.

At the end you should get a `github.pickle` file containing the PRs.

## Extract PRs as CSV
In order to feed PRs into GSheets, you must extract interesting data to CSV.

This script is an example of what was done in the main GSheet:

```
$ ./extract-csv.py
number,user,assignee,created_at,closed_at,sigs,review_s
90030,ahg-g,alculquicondor,2020-04-09 20:34:36,2020-04-10 11:11:49,sig/scheduling,52633
90015,serathius,cblecker,2020-04-09 18:19:37,2020-04-10 05:28:02,sig/instrumentation,40105
89989,tanjunchen,lavalamp,2020-04-09 02:18:10,2020-04-10 05:27:49,sig/api-machinery,11379
...
```

Once generated, the CSV can be imported in GSheet or Excel to create pivot tables and reports.

## Other scripts
There are other scripts that compute data upfront and only use CSV for displaying data.

### diff-by-sig
For each month, calculates the difference between PRs created and merged for each SIG.

We only consider PRs that are assigned to 1 SIG, otherwise they go to the category `multi` and are not graphed.

### merged-by-sig
For each month, calculates the number of PRs merged for each SIG.

We only consider PRs that are assigned to 1 SIG, otherwise they go to the category `multi` and are not graphed.

### reviewtime-by-sig
For each month, calculates the number of seconds each PR on average has spent between creation and merge.

We only consider PRs that are assigned to 1 SIG, otherwise they go to the category `multi` and are not graphed.

## Experiments
Scripts in this category were some experiments to find "smoking guns" in the review process.

### cycle-time
Try to find faster authors in merging their PRs.

### merges-assignees
Try to identify pairs of users that validate their PRs more often than the others.
