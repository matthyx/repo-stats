#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pickle
import sys
from collections import defaultdict

if len(sys.argv) < 2 or not os.path.isfile(sys.argv[1]):
    sys.exit('you must specify a pickle file to load')

with open(sys.argv[1], 'rb') as fd:
    pulls = pickle.load(fd)

cycles = defaultdict(list)
for pr in pulls.values():
    cycle = pr.closed_at-pr.created_at
    cycles[pr.user.login].append(cycle.days)

avg_cycle = {}
for k, v in cycles.items():
    if len(v) > 6:
        avg_cycle[k] = sum(v)/len(v)

for u in sorted(avg_cycle, key=lambda u: avg_cycle[u], reverse=True):
    print(avg_cycle[u], u, len(cycles[u]))
