#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pickle
import sys
from collections import defaultdict


def average(items):
    if len(items) == 0:
        return 0
    total = sum(i.seconds + i.days * 86400 for i in items)
    return total / len(items)


if len(sys.argv) < 2 or not os.path.isfile(sys.argv[1]):
    sys.exit('you must specify a pickle file to load')

with open(sys.argv[1], 'rb') as fd:
    pulls = pickle.load(fd)

months = defaultdict(lambda: defaultdict(list))
sig_set = set()
for pr in pulls.values():
    sigs = [l.name for l in pr.labels if l.name.startswith('sig/')]
    if len(sigs) == 1:
        sig = sigs[0].lstrip('sig/')
    else:
        sig = 'multi'
    sig_set.add(sig)
    closed = "{}-{:02d}".format(pr.closed_at.year, pr.closed_at.month)
    months[closed][sig].append(pr.closed_at-pr.created_at)

print('month', *sorted(sig_set), sep=',')
for month in sorted(months.keys()):
    print(month, *[average(months[month][sig])
                   for sig in sorted(sig_set)], sep=',')
