#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pickle
import sys
from collections import defaultdict

if len(sys.argv) < 2 or not os.path.isfile(sys.argv[1]):
    sys.exit('you must specify a pickle file to load')

with open(sys.argv[1], 'rb') as fd:
    pulls = pickle.load(fd)

months = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))
sig_set = set()
for pr in pulls.values():
    sigs = [l.name for l in pr.labels if l.name.startswith('sig/')]
    if len(sigs) == 1:
        sig = sigs[0].lstrip('sig/')
    else:
        sig = 'multi'
    sig_set.add(sig)
    created = "{}-{:02d}".format(pr.created_at.year, pr.created_at.month)
    closed = "{}-{:02d}".format(pr.closed_at.year, pr.closed_at.month)
    months[created][sig]['created'] += 1
    months[closed][sig]['closed'] += 1

print('month', *sorted(sig_set), sep=',')
for month in sorted(months.keys()):
    print(month, *[months[month][sig]['closed'] -
                   months[month][sig]['created'] for sig in sorted(sig_set)], sep=',')
