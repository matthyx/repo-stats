#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pickle
from collections import defaultdict
from datetime import datetime

with open('github.pickle', 'rb') as fd:
    pulls = pickle.load(fd)

newpulls = defaultdict(dict)
for k, v in pulls.items():
    newpulls[v.closed_at.year][k] = v

for y, pulls in newpulls.items():
    print(y, len(pulls))
    with open('github-{}.pickle'.format(y), 'wb') as fd:
        pickle.dump(pulls, fd)
