#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

from github import Github

with open('TOKEN') as fd:
    g = Github(fd.read().strip())
repo = 'kubernetes/kubernetes'


def print_events_for(user):
    print('Approved PRs by', user, 'in', repo)
    events = g.get_user(user).get_events()
    nb_events = 0
    for event in events:
        nb_events += 1
        try:
            if event.repo.name == repo and '/approve' in event.payload['comment']['body']:
                print(
                    event.payload['issue']['user']['login'],
                    event.payload['comment']['html_url']
                )
        except KeyError:
            continue
    print('(parsed %d events, max 300 or 90 days)' % nb_events)


def main():
    if len(sys.argv) != 2:
        sys.exit('Please specify a github username')
    print_events_for(sys.argv[1])


if __name__ == "__main__":
    main()
